/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.ads.reserva.rest.exceptions.mapper;

import java.io.Serializable;

/**
 *
 * @author kieckegard
 */
public class ExceptionReponse implements Serializable {
    
    private int statusCode;
    private String description;

    public ExceptionReponse(int statusCode, String description) {
        this.statusCode = statusCode;
        this.description = description;
    }

    public ExceptionReponse() {
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ExceptionReponse{" + "statusCode=" + statusCode + ", description=" + description + '}';
    }
}
