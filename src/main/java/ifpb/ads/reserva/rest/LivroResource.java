package ifpb.ads.reserva.rest;

import ifpb.ads.reserva.domain.Livro;
import ifpb.ads.reserva.domain.LivroLink;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * @author Ricardo Job
 * @mail ricardo.job@ifpb.edu.br
 * @since 20/07/2017, 10:41:04
 */
@Path("livro")
@Stateless
public class LivroResource {

    @PersistenceContext
    private EntityManager em;

    @Context
    private ResourceContext resourceContext;

    @GET
    @Path("{livroId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(
            @DefaultValue("1") @PathParam("livroId") int livroId,
            @Context UriInfo uriInfo) {

        Livro found = this.em.find(Livro.class, livroId);
        if (found == null) {
            throw new NotFoundException("There's no book with the id "
                    + livroId);
        }

        LivroLink livroLink = LivroLink.of(found, uriInfo);

        return Response
                .ok(livroLink)
                .build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getLivros(@Context UriInfo uriInfo) {

        List<Livro> livros = em.createQuery("FROM Livro l", Livro.class).getResultList();

        List<LivroLink> livrosLinks = livros
                .stream()
                .map(l -> {
                    return LivroLink.of(l, uriInfo);
                })
                .collect(Collectors.toList());
        
        GenericEntity<List<LivroLink>> entity = new GenericEntity<List<LivroLink>>(livrosLinks) {
        };

        return Response
                .ok()
                .entity(entity)
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response novoLivro(
            Livro livro,
            @Context UriInfo uriInfo) {

        //Não façam isso em casa!
        em.persist(livro);
        String id = String.valueOf(livro.getId());
        URI location = uriInfo.getBaseUriBuilder() // ../api
                .path(LivroResource.class) // ../api/livro
                .path(id) // ../api/livro/id
                .build();

        return Response
                .created(location)
                .entity(livro)
                .build();

    }

    @Path("{livroId}/autor")
    public LivroAutorResource authorSubResource() {
        return resourceContext.getResource(LivroAutorResource.class);
    }

}
