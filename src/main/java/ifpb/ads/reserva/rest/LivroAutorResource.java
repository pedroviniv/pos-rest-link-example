/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.ads.reserva.rest;

import ifpb.ads.reserva.domain.Autor;
import ifpb.ads.reserva.domain.Livro;
import java.util.List;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author kieckegard
 */
@Stateless
public class LivroAutorResource {

    @PersistenceContext
    private EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAutores(
            @DefaultValue("-1")
            @PathParam("livroId") int livroId,
            @Context UriInfo uriInfo) {
        
        Livro livroFound = this.em.find(Livro.class, livroId);
        
        if (livroFound == null)
            throw new NotFoundException("There's no livro with the id " + livroId);
        
        GenericEntity<List<Autor>> entity =
                new GenericEntity<List<Autor>>(livroFound.getAutores()){};
        
        return Response
                .ok(entity)
                .build();
    }

    @GET
    @Path("{autorId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAutor(
            @PathParam("livroId") int livroId,
            @PathParam("autorId") int autorId) {

        Livro livro = this.em.find(Livro.class, livroId);
        Optional<Autor> result = livro.getAutores().stream()
                .filter(a -> a.getId() == autorId)
                .findFirst();

        if (!result.isPresent()) {

            return Response.noContent().build();
        }

        return Response
                .ok(result.get())
                .build();
    }

    @PUT
    @Path("{autorId}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response addAutor(
            @PathParam("livroId") int idLivro,
            @PathParam("autorId") int idAutor) {

        Livro livro = em.find(Livro.class, idLivro);
        Autor autor = em.find(Autor.class, idAutor);
        livro.adicionarAutor(autor);
        return Response.ok(livro).build();
    }

}
