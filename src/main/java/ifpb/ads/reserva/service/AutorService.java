/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.ads.reserva.service;

import ifpb.ads.reserva.domain.Autor;

/**
 *
 * @author kieckegard
 */
public interface AutorService {
    
    Autor findById(Integer id);
    void persist(Autor autor);
}
