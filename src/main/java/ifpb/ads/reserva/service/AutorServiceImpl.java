/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.ads.reserva.service;

import ifpb.ads.reserva.domain.Autor;
import ifpb.ads.reserva.rest.exceptions.AutorException;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kieckegard
 */

@Local(AutorService.class)
@Stateless
public class AutorServiceImpl implements AutorService {
    
    @PersistenceContext
    private EntityManager manager; // ../api/autor/id

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Override
    public Autor findById(Integer id) {
        Autor found = this.manager.find(Autor.class, id);
        if(found == null)
            throw new AutorException("There's no author with the id " + id);
        return found;
    }

    @Override
    public void persist(Autor autor) {
        this.manager.persist(autor);
    }
    
}
