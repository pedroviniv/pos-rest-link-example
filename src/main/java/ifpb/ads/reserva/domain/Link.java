/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.ads.reserva.domain;

import java.io.Serializable;

/**
 *
 * @author kieckegard
 */
public class Link implements Serializable {
    
    private String title;
    private String href;
    private String rel;

    public Link(String title, String href, String rel) {
        this.title = title;
        this.href = href;
        this.rel = rel;
    }

    public Link() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    @Override
    public String toString() {
        return "Link{" + "title=" + title + ", href=" + href + ", rel=" + rel + '}';
    }
}
