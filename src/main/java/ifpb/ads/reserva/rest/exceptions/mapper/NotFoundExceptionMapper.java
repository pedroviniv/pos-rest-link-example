/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.ads.reserva.rest.exceptions.mapper;

import ifpb.ads.reserva.rest.exceptions.AutorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author kieckegard
 */

@Provider
public class NotFoundExceptionMapper 
        implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(NotFoundException exception) {
        
        ExceptionReponse response = 
                new ExceptionReponse(404, exception.getMessage());
        
        return Response
                .status(Status.NOT_FOUND)
                .entity(response)
                .build();
    }
    
}
