/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.ads.reserva.domain;

import ifpb.ads.reserva.rest.AutorResource;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kieckegard
 */

@XmlRootElement
public class LivroLink implements Serializable {

    private String edicao;
    private String titulo;
    private String descricao;
    private List<Link> autores = new ArrayList<>();

    private LivroLink(String edicao, String titulo, String descricao) {
        this.edicao = edicao;
        this.titulo = titulo;
        this.descricao = descricao;
    }
    
    public LivroLink() {
        
    }

    public static LivroLink of(Livro livro, UriInfo uriInfo) {
        LivroLink livroLink = new LivroLink(livro.getEdicao(),
                livro.getTitulo(), livro.getDescricao());

        List<Link> autorLinks = livro.getAutores()
                .stream()
                .map((a) -> {
                    return createLinkByAuthor(a, uriInfo);
                })
                .collect(Collectors.toList());
        livroLink.setAutores(autorLinks);

        return livroLink;
    }

    private static Link createLinkByAuthor(Autor autor, UriInfo uriInfo) {
        URI uri = uriInfo.getBaseUriBuilder()
                .path(AutorResource.class)
                .path(String.valueOf(autor.getId()))
                .build();
        return new Link(autor.getNome(), uri.toString(), "autor");
    }

    public String getEdicao() {
        return edicao;
    }

    public void setEdicao(String edicao) {
        this.edicao = edicao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Link> getAutores() {
        return autores;
    }

    public void setAutores(List<Link> autores) {
        this.autores = autores;
    }

    @Override
    public String toString() {
        return "LivroLink{" + "edicao=" + edicao + ", titulo=" + titulo + ", descricao=" + descricao + ", autores=" + autores + '}';
    }
}
