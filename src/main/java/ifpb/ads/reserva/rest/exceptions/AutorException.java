/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.ads.reserva.rest.exceptions;

/**
 *
 * @author kieckegard
 */
public class AutorException extends RuntimeException {

    public AutorException(String message) {
        super(message);
    }

    public AutorException(String message, Throwable cause) {
        super(message, cause);
    }

    public AutorException(Throwable cause) {
        super(cause);
    }
}
