package ifpb.ads.reserva.rest;

import ifpb.ads.reserva.domain.Autor;
import ifpb.ads.reserva.rest.exceptions.AutorException;
import ifpb.ads.reserva.service.AutorService;
import java.net.URI;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * @author Ricardo Job
 * @mail ricardo.job@ifpb.edu.br
 * @since 20/07/2017, 10:41:04
 */
@Path("autor")
@Stateless
public class AutorResource {

    @EJB
    private AutorService service;

    @GET
    @Path("{autorId}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getById(@PathParam("autorId") Integer autorId) {

        try {
        Autor result = this.service.findById(autorId);

        return Response
                .ok()
                .entity(result)
                .build();
        } catch (AutorException ex) {
            throw new NotFoundException(ex.getMessage(), ex);
        }

    }

    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response novoAutor(
            Autor autor,
            @Context UriInfo uriInfo) {

        this.service.persist(autor);
        String id = String.valueOf(autor.getId());
        URI location = uriInfo.getBaseUriBuilder()
                .path(AutorResource.class)
                .path(id)
                .build();

        return Response
                .created(location)
                .entity(autor)
                .build();

    }
}
