/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.ads.reserva.rest;

import ifpb.ads.reserva.domain.Livro;
import ifpb.ads.reserva.domain.Reserva;
import java.net.URI;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author kieckegard
 */

@Path("reserva")
@Stateless
public class ReservaResource {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @GET
    @Path("{reservaId}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response find(@PathParam("reservaId") int reservaId) {
        
        Reserva reserva = this.entityManager
                .find(Reserva.class, reservaId);
        
        return Response
                .ok(reserva)
                .build();
    }
    
    @POST
    @Path("{clientId}")
    public Response newReserva(@Context UriInfo uriInfo, @PathParam("clientId") String client) {
        
        Reserva reserva = Reserva.of(client);
        this.entityManager.persist(reserva);
        
        URI uri = uriInfo.getBaseUriBuilder()
                .path(ReservaResource.class)
                .path(String.valueOf(reserva.getId()))
                .build();
        
        return Response
                .created(uri)
                .build();
    }
    
    @PUT
    @Path("{reservaId}/livro/{livroId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addBook(
            @PathParam("reservaId") int reservaId, 
            @PathParam("livroId") int livroId) {
        
        Reserva reservaFound = this.entityManager
                .find(Reserva.class, reservaId);
        Livro livroFound = this.entityManager
                .find(Livro.class, livroId);
        
        reservaFound.adicionarLivro(livroFound);
        
        return Response
                .ok(reservaFound)
                .build();
    }
    
    @PUT
    @Path("{reservaId}")
    public Response changeStatus(
            @PathParam("reservaId") int reservaId, 
            @QueryParam("status") String status) {
        
        Reserva reservaFound = this.entityManager
                .find(Reserva.class, reservaId);
        
        switch(status) {
            case "agendar":
                reservaFound.agendar();
                break;
            case "concluir":
                reservaFound.concluir();
                break;
        }
        
        return Response
                    .ok(reservaFound)
                    .build();
    }    
}
